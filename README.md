# pbihb_harrison

This repository contains the analysis plans for the Perception of Breathing In the Human Brain (PBIHB) project. This project examines both behavioural and functional brain responses to breathing tasks in two groups of individuals with low and moderate levels of anxiety.
PI: Olivia Harrison
